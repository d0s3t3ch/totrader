from os import getenv
from dotenv import load_dotenv
from requests import get as requests_get
from requests import post as requests_post
from requests.auth import HTTPBasicAuth


class TradeOgre(object):
    def __init__(self):
        load_dotenv('.env')
        self.base = 'https://tradeogre.com/api/v1'
        self.auth = HTTPBasicAuth(
            getenv('TO_USER'),
            getenv('TO_PASS')
        )

    def req(self, route, method='get', data={}):
        url = self.base + route
        if method == 'get':
            r = requests_get(url, auth=self.auth)
        else:
            r = requests_post(url, auth=self.auth, data=data)
        return r.json()

    def get_trade_pair(self, pair):
        route = f'/ticker/{pair}'
        return self.req(route)

    def get_balance(self, currency):
        route = '/account/balance'
        data = {'currency': currency}
        return self.req(route, 'post', data)

    def get_balances(self):
        route = '/account/balances'
        return self.req(route)

    def submit_order(self, type, pair, quantity, price):
        route = f'/order/{type}'
        data = {'market': pair, 'quantity': quantity, 'price': price}
        return self.req(route, 'post', data)

    def get_order(self, uuid):
        route = f'/account/order/{uuid}'
        return self.req(route)

    def get_orders(self, pair):
        route = f'/account/orders'
        data = {'market': pair}
        return self.req(route, 'post', data)
    
    def get_bitcoin_price(self):
        url = 'https://api.coingecko.com/api/v3/coins/bitcoin'
        headers = {'accept': 'application/json'}
        data = {
            'localization': False,
            'tickers': False,
            'market_data': True,
            'community_data': False,
            'developer_data': False,
            'sparkline': False
        }
        r = requests_get(url, headers=headers, data=data)
        return r.json()['market_data']['current_price']['usd']

