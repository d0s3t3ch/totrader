from os import getenv
from dotenv import load_dotenv
from peewee import *
from datetime import datetime, timedelta


load_dotenv('.env')

db = PostgresqlDatabase(
    getenv('DB_NAME', 'trader'),
    user=getenv('DB_USER'),
    password=getenv('DB_PASS'),
    host=getenv('DB_HOST', '127.0.0.1'),
    port=getenv('DB_PORT', 5432)
)


class Ticker(Model):
    id = AutoField()
    trade_pair = CharField()
    initial_price = DoubleField()
    current_price = DoubleField()
    high_price = DoubleField()
    low_price = DoubleField()
    volume = DoubleField()
    bid = DoubleField()
    ask = DoubleField()
    spread_btc = DoubleField()
    spread_sats = IntegerField()
    spread_perc = DoubleField()
    date = DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db

class Balance(Model):
    total = DoubleField()
    available = DoubleField()
    currency = CharField()
    date = DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db

class Portfolio(Model):
    usd = DoubleField()
    btc = DoubleField()
    date = DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db

class BitcoinPrice(Model):
    price_usd = DoubleField()
    date = DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db

class Order(Model):
    trade_pair = CharField()
    trade_type = CharField()
    buy = BooleanField()
    quantity = DoubleField()
    price = DoubleField()
    uuid = TextField(null=True)
    active = BooleanField(default=True)
    cancelled = BooleanField(default=False)
    date = DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db

class Earning(Model):
    trade_pair = CharField()
    quantity = DoubleField()
    date = DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db

db.create_tables([Ticker, Balance, Order, Earning, BitcoinPrice, Portfolio])
